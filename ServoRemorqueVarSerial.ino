/*
 Controlling a servo position using a potentiometer (variable resistor)
 by Michal Rinott <http://people.interaction-ivrea.it/m.rinott>

 modified on 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Knob
*/

#include <Servo.h>

Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo

int Angle = 90;

void setup() 
{
    // initialize serial:
  Serial.begin(9600);
  myservo1.attach(6);  // attaches the servo on pin 6 to the servo object
  myservo2.attach(10);  // attaches the servo on pin 10 to the servo object
}

void loop() 
{
  while (Serial.available()) {  

    Angle=Serial.parseInt();
    Serial.read();

    myservo1.write(Angle);
    //delay(500);   

    myservo2.write(Angle);
    //delay(500); 
    Serial.print(Angle); 
    Serial.println(":OK");
  }
}



